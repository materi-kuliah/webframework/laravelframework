<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\BarangModel;
use Hash;
use Auth;
use Session;

class AuthApi extends Controller
{
    public function userLogin(Request $request, User $vol){
        $email = $request->email;
        $pass = $request->password;

        
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //user sent their email 
            Auth::attempt(['email' => $email, 'password' => $pass]);
        } 

        if ( Auth::check() ) {
            $vol = $vol->find(Auth::user()->id);
            $res = [
                'status'    => 1,
                'code'      => http_response_code(), 
                'message'   => '',
                'data'      => $vol
            ];
            return response()->json($res);
        }else{
            $res = [
                'status'    => 0,
                'code'      => http_response_code(), 
                'message'   => 'User atau Password tidak sesuai',
            ];
            return response()->json($res);
        }
    }

    public function getDataBarang(Request $request){
        $nama = $request->namaBarang;
        if(!empty($nama)){
            $barang = BarangModel::where('nama_barang','LIKE','%'.$nama.'%')
                      ->get();
        }else{
            $barang = BarangModel::All();
        }

        // dd($barang);
        if(count($barang)>0){
            $res = [
                'status'    => 1,
                'code'      => http_response_code(), 
                'message'   => '',
                'data'      => $barang
            ];
        }else{
            $res = [
                'status'    => 0,
                'code'      => http_response_code(), 
                'message'   => 'Data tidak ditemukan',
            ];
        }
        return response()->json($res);
    }
}
